# acme-colors

Plan 9 vim colorscheme which supports 256 colors.
Made some slight modifications, [here's the original repo on GitHub][https://github.com/plan9-for-vimspace/acme-colors]
- Tweaked CursorLine colors and added Pmenu
